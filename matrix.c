#include<stdio.h>
int main()
{
	int row, col, a[100][100], b[100][100], sum[100][100], mul[100][100];
	printf("\nEnter number of rows: ");
	scanf("%d", &row);
	printf("\nEnter number of columns : ");
	scanf("%d", &col);
	printf("\nElements of matrix a : \n");
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			printf("\nEnter element a%d%d : ", x, y);
			scanf("%d", &a[x][y]);
		}
	}
	printf("\n\nElements of matrix b : \n");
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			printf("\nEnter element b%d%d : ", x, y);
			scanf("%d", &a[x][y]);
		}
	}
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			sum[x][y] = a[x][y] + b[x][y];
		}
	}
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			mul[x][y] = 0;
		}
	}
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			for(int z = 0 ; z < col ; z++)
			{
				mul[x][y] += a[x][z] * b[z][y];
			}
		}
	}
	printf("\n\nAddition : \n\n");
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			printf("%d ", sum[x][y]);
			if(y == col - 1)
			{
				printf("\n");
			}
		}
	}
	printf("\n\nMultiplication : \n\n");
	for(int x = 0 ; x < row ; x++)
	{
		for(int y = 0 ; y < col ; y++)
		{
			printf("%d ", mul[x][y]);
			if(y == col - 1)
			{
				printf("\n");
			}
		}
	}
	return 0;
}