#include<stdio.h>
#include<string.h>
#define SIZE 100
int main()
{
	char word[SIZE];
	char cha;
	printf("\nEnter a string: ");
	fgets(word, SIZE, stdin);
	printf("\nEnter a character : ");
	scanf("%c", &cha);
	int len = strlen(word);
	int freq = 0;
	for(int n = len ; n >= 0 ; n--)
	{
		if(word[n] == cha)
		{
			freq = freq + 1;
		}
	}
	printf("\nFrequency of %c is %d.\n\n", cha, freq);
	return 0;
}